<?php
/**
 * The file for displaying all custom meta boxes in custom posts.
 *
 * @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
 * @package _s
 * @author Akshay Saxena
 */

/**
 * Adds a metabox to the right side of the screen under the featured image box
 */
add_action('add_meta_boxes',function (){

    /**
     * Custom metaboxes for Featured Offers custom post
     */
    add_meta_box('featured-button-text','Featured Slider Button Text','wpt_save_meta','featured-offers','side');

    add_meta_box('featured-button-link','Featured Slider Button Link','wpt_save_link_meta','featured-offers','side');

    /**
     * Custom metaboxes for CTA custom post
     */
    add_meta_box('cta-button-link','CTA Button url','wpt_save_cta_link_meta','call-to-action','side');

    /**
     * Custom metaboxes for Value Propositions custom post
     */
    add_meta_box('propositions-link','Propositions Button url','wpt_save_value_proposition_link_meta','propositions','side');

    /**
     * Custom metaboxes for Testimonials custom post
     */
    add_meta_box('designation','Client Designation','wpt_save_designation_meta','testimonials','side');

    /**
     * Custom metaboxes for TEST TES post
     */
    add_meta_box('categories_meta_box','Slider Visible on Pages','custom_term_function','featured-offers','side');
});

/**
 * Triggers metabox callback function when save/edit of button text field
 */
function wpt_save_meta($post){
    $featured_text_val = get_post_meta($post->ID);
    wp_nonce_field( 'featured_text_check', 'featured_text_check_value' );
    ?>
    <input type="text" name="featured-text"
     value="<?php if( isset ( $featured_text_val['featured-text'])) echo $featured_text_val['featured-text'][0] ?>" >

    <?php

}
/**
 * Triggers metabox callback function when save/edit of button link field
 */
function wpt_save_link_meta($post){
    $featured_button_link = get_post_meta($post->ID);
    ?>

     <input type="text" name="featured-button-link"
     value="<?php if( isset ( $featured_button_link['featured-button-link'])) echo $featured_button_link['featured-button-link'][0] ?>" >
    <?php

}

/**
 * Triggers metabox callback function when save/edit of CTA  link field
 */
function wpt_save_cta_link_meta($post){
    $cta_button_link = get_post_meta($post->ID);
    ?>

     <input type="text" name="cta-button-link"
     value="<?php if( isset ( $cta_button_link['cta-button-link'])) echo $cta_button_link['cta-button-link'][0] ?>" >
    <?php

}

/**
 * Triggers metabox callback function when save/edit of value proposition link
 */
function wpt_save_value_proposition_link_meta($post){
    $propositions_link = get_post_meta($post->ID);
    ?>

     <input type="text" name="propositions_link"
     value="<?php if( isset ( $propositions_link['propositions-link'])) echo $propositions_link['propositions-link'][0] ?>" >
    <?php

}

/**
 * Triggers metabox callback function when save/edit of client designation
 */
function wpt_save_designation_meta($post){
    $designation = get_post_meta($post->ID);
    ?>

     <input type="text" name="designation"
     value="<?php if( isset ( $designation['designation'])) echo $designation['designation'][0] ?>" >
    <?php

}

/**
 * Show/Save Woocommerce product categories
 */

function custom_term_function($post){
    $cats = get_post_meta($post->ID,'categories_meta_box',true);

    $terms = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
) );

    echo '<ul>';
    foreach( $terms as $category):
        $cat_id = $category->term_id;
            $checked = (in_array($cat_id,(array)$cats)? ' checked="checked"': "");
           echo'<li id="cat-'.$cat_id.'"><input type="checkbox" name="categories_meta_box[]" id="'.$cat_id.'" value="'.$cat_id.'"'.$checked.'> <label for="'.$cat_id.'">'.__($category->name, 'pages_textdomain' ).'</label></li>';
    endforeach;
    echo '</ul>';

}

/**
 * Save metabox values
 */
add_action('save_post',function($post_id){

/**
 * Save metabox values for Featured custom post slider
 */

    if(isset($_POST['featured-text'])){

        update_post_meta($post_id,'featured-text',$_POST['featured-text']);
    }

    if(isset($_POST['featured-button-link'])){

        update_post_meta($post_id,'featured-button-link',$_POST['featured-button-link']);
    }

    if(isset($_POST['categories_meta_box'])){

        update_post_meta($post_id,'categories_meta_box',$_POST['categories_meta_box']);
    }

/**
 * Save metabox values for CTA Banner custom post types
 */

    if(isset($_POST['cta-button-link'])){

        update_post_meta($post_id,'cta-button-link',$_POST['cta-button-link']);
    }

/**
 * Save metabox values for Value proposition custom post types
 */

    if(isset($_POST['propositions-link'])){

        update_post_meta($post_id,'propositions-link',$_POST['propositions-link']);
    }

/**
 * Save metabox values for Customer testimonials custom post types
 */

    if(isset($_POST['designation'])){

        update_post_meta($post_id,'designation',$_POST['designation']);
    }

});

/**
 * show meta value after post content
 */
add_filter('the_content',function($content){

    $meta_val = get_post_meta(get_the_ID(),'featured-text',true);
    return $meta_val;

    $featured_link_val = get_post_meta(get_the_ID(),'featured-button-link',true);
    return $featured_link_val;

    $cta_link_val = get_post_meta(get_the_ID(),'cta-button-link',true);
    return $cta_link_val;

    $prop_link_val = get_post_meta(get_the_ID(),'propositions-link',true);
    return $prop_link_val;

    $designation = get_post_meta(get_the_ID(),'designation',true);
    return $designation;

    $categories_meta_box = get_post_meta(get_the_ID(),'categories_meta_box',true);
    return $categories_meta_box;

});

