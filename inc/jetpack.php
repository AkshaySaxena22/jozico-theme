<?php
/**
 * Jetpack compatibility file.
 *
 * @link https://jetpack.com/
 * @package _s
 * @author Vinny Bhaskar
 */

/**
 * Jetpack setup function.
 *
 * @see https://jetpack.com/support/infinite-scroll/
 * @see https://jetpack.com/support/responsive-videos/
 * @see https://jetpack.com/support/content-options/
 */
function _s_jetpack_setup()
{
    // Add theme support for infinite scroll.
    add_theme_support('infinite-scroll', [
        'container' => 'main',
        'render' => '_s_infinite_scroll_render',
        'footer' => 'page',
    ]);

    // Add theme support for responsive videos.
    add_theme_support('jetpack-responsive-videos');

    // Add theme support for content options.
    add_theme_support('jetpack-content-options', [
        'post-details' => [
            'stylesheet' => '_s-style',
            'date' => '.posted-on',
            'categories' => '.cat-links',
            'tags' => '.tags-links',
            'author' => '.byline',
            'comment' => '.comments-link',
        ],
        'featured-images' => [
            'archive' => true,
            'post' => true,
            'page' => true,
        ],
    ]);
}

add_action('after_setup_theme', '_s_jetpack_setup');

/**
 * Custom render function for infinite scroll.
 */
function _s_infinite_scroll_render()
{
    while (have_posts()) {
        the_post();
        if (is_search()) {
            get_template_part('template-parts/content', 'search');
        } else {
            get_template_part('template-parts/content', get_post_type());
        }
    }
}
