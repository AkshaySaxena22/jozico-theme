<?php
/**
 * Functions which enhance the theme by hooking into WordPress.
 *
 * @package _s
 * @author Vinny Bhaskar
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function _s_body_classes($classes)
{
    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
        $classes[] = 'hfeed';
    }

    // Adds no-sidebar class when there is no sidebar present.
    if (!is_active_sidebar('sidebar-1')) {
        $classes[] = 'no-sidebar';
    }

    $full_width_page_templates = [
        'templates/template-design-system.php',
        'templates/template-full-width.php',
    ];

    // Adds no-sidebar class on pages requiring full width support.
    if (is_page_template($full_width_page_templates)) {
        $classes[] = 'no-sidebar';
    }

    return $classes;
}

add_filter('body_class', '_s_body_classes');

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function _s_pingback_header()
{
    if (is_singular() && pings_open()) {
        printf('<link rel="pingback" href="%s">', esc_url(get_bloginfo('pingback_url')));
    }
}

add_action('wp_head', '_s_pingback_header');
