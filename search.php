<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * @package _s
 * @author Vinny Bhaskar
 */

get_header();
?>

    <section id="primary" class="content-area">
        <main id="main" class="site-main">

        <?php if (have_posts()) { ?>
            <header class="page-header">
                <h1 class="page-title">
                    <?php
                    /* translators: %s: Search query */
                    printf(esc_html__('Search Results for: %s', '_s'), '<span>' . get_search_query() . '</span>');
                    ?>
                </h1>
            </header><!-- .page-header -->

            <?php
            // Start the loop.
            while (have_posts()) :
                the_post();

                /*
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a
                 * file called content-search.php and that will be used instead.
                 */
                get_template_part('template-parts/content', 'search');
            endwhile; // End of the loop.

            the_posts_navigation();
        } else {
            get_template_part('template-parts/content', 'none');
        }
        ?>

        </main><!-- #main -->
    </section><!-- #primary -->

<?php
get_sidebar();
get_footer();
