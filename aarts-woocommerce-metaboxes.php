<?php
/**
 * The file for displaying all custom meta boxes in Woocommerce product page.
 *
 * @link https://docs.woocommerce.com/document/add-a-project-meta-field
 * @package _s
 * @author Akshay Saxena
 */

/**
 * Adding a custom Meta container to admin products pages
 */
add_action( 'add_meta_boxes', 'create_custom_meta_box' );
if ( ! function_exists( 'create_custom_meta_box' ) )
{
    function create_custom_meta_box()
    {
        add_meta_box(
            'custom_product_meta_box',
            __( 'Additional Product Information <em>(optional)</em>', 'cmb' ),
            'add_custom_content_meta_box',
            'product',
            'normal',
            'default'
        );
    }
}

/**
 * Custom metabox content in admin product pages
 */
if ( ! function_exists( 'add_custom_content_meta_box' ) ){
    function add_custom_content_meta_box( $post ){
        $prefix = '_bhww_'; // global $prefix;

        $additional_info_field = get_post_meta($post->ID, $prefix.'additional_info_field_wysiwyg', true) ? get_post_meta($post->ID, $prefix.'additional_info_field_wysiwyg', true) : '';
        $benefits = get_post_meta($post->ID, $prefix.'benefits_wysiwyg', true) ? get_post_meta($post->ID, $prefix.'benefits_wysiwyg', true) : '';
        $args['textarea_rows'] = 6;

        echo '<p>'.__( 'Additional product information', 'cmb' ).'</p>';
        wp_editor( $additional_info_field, 'additional_info_field_wysiwyg', $args );

        echo '<p>'.__( 'Benefits', 'cmb' ).'</p>';
        wp_editor( $benefits, 'benefits_wysiwyg', $args );

        echo '<input type="hidden" name="custom_product_field_nonce" value="' . wp_create_nonce() . '">';
    }
}

/**
 * Save the data of the Meta field
 */
add_action( 'save_post', 'save_custom_content_meta_box', 10, 1 );
if ( ! function_exists( 'save_custom_content_meta_box' ) )
{

    function save_custom_content_meta_box( $post_id ) {
        $prefix = '_bhww_'; // global $prefix;

        // We need to verify this with the proper authorization (security stuff).

        // Check if our nonce is set.
        if ( ! isset( $_POST[ 'custom_product_field_nonce' ] ) ) {
            return $post_id;
        }
        $nonce = $_REQUEST[ 'custom_product_field_nonce' ];

        //Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce ) ) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        // Check the user's permissions.
        if ( 'product' == $_POST[ 'post_type' ] ){
            if ( ! current_user_can( 'edit_product', $post_id ) )
                return $post_id;
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) )
                return $post_id;
        }

        // Sanitize user input and update the meta field in the database.
        update_post_meta( $post_id, $prefix.'additional_info_field_wysiwyg', wp_kses_post($_POST[ 'additional_info_field_wysiwyg' ]) );
        update_post_meta( $post_id, $prefix.'benefits_wysiwyg', wp_kses_post($_POST[ 'benefits_wysiwyg' ]) );
    }
}